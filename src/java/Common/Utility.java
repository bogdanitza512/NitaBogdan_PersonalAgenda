package Common;

import Models.Model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Utility {

    public final static class Stringify {
        public final static class JSON {

            public static final String OBJ_START = "{\n\t";
            public static final String OBJ_END = "\n}";
            public static final String DELIMITER = ",\n\t";
//            public static final String ARR_START = "[\n";
//            public static final String ARR_END = "\n]";


            public static String compose(List<String> fields) {
                return OBJ_START + join(fields) + OBJ_END;
            }

            public static String append(String original, List<String> fields) {
                return original.endsWith(OBJ_END)?
                        add(original, fields) :
                        compose(fields);
            }

            private static String join(List<String> fields) {
                return String.join(DELIMITER, fields);
            }

            private static String add(String original, List<String> fields) {
                return open(original) + DELIMITER + join(fields) + OBJ_END;
            }

            private static String open(String original) {
                return original.substring(0, original.length() - OBJ_END.length());
            }

            public static final class Field {

                private static Set<Class> primitives = Set.of(
                    Boolean.class,
                    Integer.class,
                    Float.class
                );

                public static String of(String key, Object value) {
                    return wrap(key) + ": " + wrap(value);
                }

                private static String wrap(String str) {
                    return "\"" + str + "\"";
                }

                private static String wrap(Object val) {
                    return !primitives.contains(val.getClass()) ?
                            wrap(val.toString()) :
                            val.toString();
                }
            }

        }

        public static final class CSV {

            public static final String DELIMITER = ",";
            public static final String NEWLINE = "\n";

            public static String compose(List<Object> fields) {
                return fields
                    .stream()
                    .map(Object::toString)
                    .collect(Collectors.joining(DELIMITER))
                    + NEWLINE;
            }

            public static String[] split(String dump) {
                return dump.split(DELIMITER);
            }
        }

    }

    @SafeVarargs
    public static<T> List<T> concatenate(List<T>... lists)
    {
        return Stream.of(lists)
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

}
