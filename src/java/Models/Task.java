package Models;

import java.lang.*;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import Models.Model;

import Fields.Field;
import Fields.IntegerField;
import Fields.StringField;


import Common.Utility.Stringify.*;

public class Task extends Model {
    private static int counter;

    private IntegerField id = new IntegerField("ID", counter++);

    private StringField name = new StringField("name").withValue("Name");
    private StringField details = new StringField("details").withValue("Details");

    public Task() {
        super();
    }

    public Task(String name, String details) {
        super();

        this.name.setValue(name);
        this.details.setValue(details);
    }

    public String getName() {
        return name.getValue();
    }

    public void setName(String name) { this.name.setValue(name); }

    public String getDetails() {
        return details.getValue();
    }

    public void setDetails(String details) { this.details.setValue(details); }

    public Task withName(String name) {
        this.setName(name);
        return this;
    }

    public Task withDetails(String details) {
        this.setDetails(details);
        return this;
    }

    @Override
    public int getID() {
        return id.getValue();
    }

    @Override
    public List<Field<?>> getFields() {
        return List.of(
            id, name, details
        );
    }

    @Override
    public String toString() {
        return JSON.compose(
            getFields()
                .stream()
                .map(field -> JSON.Field.of(field.getKey(), field.getValue()))
                .collect(Collectors.toList())
        );
    }
}
