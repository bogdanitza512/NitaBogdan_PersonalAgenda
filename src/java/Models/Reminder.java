package Models;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import Fields.*;

import static Common.Utility.concatenate;

public class Reminder extends Task {

    private DateField startDate = new DateField("startDate");
    private DurationField interval = new DurationField("interval");
    private DateField endDate = new DateField("endDate");

    public Reminder() {
        super();
    }

    public Reminder(String name, String details, LocalDate startDate, Duration interval, LocalDate endDate) {
        super(name, details);

        this.startDate.setValue(startDate);
        this.interval.setValue(interval);
        this.endDate.setValue(endDate);
    }

    public LocalDate getStartDate() {
        return startDate.getValue();
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate.setValue(startDate);
    }

    public Duration getInterval() {
        return interval.getValue();
    }

    public void setInterval(Duration interval) {
        this.interval.setValue(interval);
    }

    public LocalDate getEndDate() {
        return endDate.getValue();
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate.setValue(endDate);
    }

    @Override
    public Reminder withName(String name) {
        return (Reminder) super.withName(name);
    }

    @Override
    public Reminder withDetails(String details) {
        return (Reminder) super.withDetails(details);
    }

    public Reminder withStartDate(LocalDate startDate) {
        this.setStartDate(startDate);
        return this;
    }

    public Reminder withInterval(Duration interval) {
        this.setInterval(interval);
        return this;
    }

    public Reminder withEndDate(LocalDate endDate) {
        this.setEndDate(endDate);
        return this;
    }

    @Override
    public List<Field<?>> getFields() {
        return concatenate(
                super.getFields(),
                List.of(
                    startDate, interval, endDate
                )
        );
    }

}
