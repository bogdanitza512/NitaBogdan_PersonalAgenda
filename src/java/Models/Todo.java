package Models;

import Fields.DateField;
import Fields.Field;
import Fields.IntegerListField;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import static Common.Utility.concatenate;

public class Todo extends Task {

    private IntegerListField subTasks = new IntegerListField("subTasks");
    private DateField deadline = new DateField("deadline");

    public Todo() {
        super();
    }

    public Todo(String name, String details, List<Integer> subTasks, LocalDate deadline) {
        super(name, details);

        this.subTasks.setValue(subTasks);
        this.deadline.setValue(deadline);
    }

    public List<Integer> getSubTasks() {
        return subTasks.getValue();
    }

    public void setSubTasks(List<Integer> subTasks) {
        this.subTasks.setValue(subTasks);
    }

    public LocalDate getDeadline() {
        return deadline.getValue();
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline.setValue(deadline);
    }

    @Override
    public Todo withName(String name) {
        return (Todo) super.withName(name);
    }

    @Override
    public Todo withDetails(String details) {
        return (Todo) super.withDetails(details);
    }

    public Todo withSubTasks(List<Integer> subTasks) {
        this.setSubTasks(subTasks);
        return this;
    }

    public Todo withDeadline(LocalDate deadline) {
        this.setDeadline(deadline);
        return this;
    }

    @Override
    public List<Field<?>> getFields() {
        return concatenate(
            super.getFields(),
            List.of(
                subTasks, deadline
            )
        );
    }
}
