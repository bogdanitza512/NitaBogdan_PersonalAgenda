package Models;

import Models.Model;

import Fields.Field;
import Fields.IntegerField;
import Fields.StringField;

import Common.Utility.Stringify.*;

import java.util.List;
import java.util.stream.Collectors;

public class Contact extends Model {

    private static int counter;

    private IntegerField id = new IntegerField("ID").withValue(counter++);

    private StringField firstName = new StringField("firstName").withValue("First Name");
    private StringField lastName = new StringField("lastName").withValue("Last Name");
    private StringField phoneNumber = new StringField("phoneNumber").withValue("0712 345 789");

    public Contact(String firstName, String lastName, String phoneNumber) {
        super();

        this.firstName.setValue(firstName);
        this.lastName.setValue(lastName);
        this.phoneNumber.setValue(phoneNumber);
    }

    public Contact(int id, String firstName, String lastName, String phoneNumber) {
        this(firstName, lastName, phoneNumber);
        this.id.setValue(id);
    }

    public Contact() {
        super();
    }

    public String getFirstName() {
        return firstName.getValue();
    }

    public void setFirstName(String firstName) {
        this.firstName.setValue(firstName);
    }

    public String getLastName() {
        return lastName.getValue();
    }

    public void setLastName(String lastName) {
        this.lastName.setValue(lastName);
    }

    public String getPhoneNumber() {
        return phoneNumber.getValue();
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber.setValue(phoneNumber);
    }

    public Contact withFirstName(String firstName) {
        this.setFirstName(firstName);
        return this;
    }

    public Contact withLastName(String lastName) {
        this.setLastName(lastName);
        return this;
    }

    public Contact withPhoneNumber(String phoneNumber) {
        this.setPhoneNumber(phoneNumber);
        return this;
    }

    @Override
    public int getID() {
        return id.getValue();
    }

    @Override
    public List<Field<?>> getFields() {
        return List.of(
            id, firstName, lastName, phoneNumber
        );
    }

    @Override
    public String toString() {
        return JSON.compose(
            getFields()
                .stream()
                .map(field -> JSON.Field.of(field.getKey(), field.getValue()))
                .collect(Collectors.toList())
        );
    }
}
