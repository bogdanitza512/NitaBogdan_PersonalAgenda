package Models;

import Common.Utility.Stringify.*;
import Fields.Field;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class Model {

    abstract public int getID();
    abstract public List<Field<?>> getFields();

    public String getHeader() {
        assert getFields() != null;

        return CSV.compose(
            getFields()
                .stream()
                .map(Field::getKey)
                .collect(Collectors.toList())
        );
    }

     public String serialize() {
        assert  getFields() != null;

        return CSV.compose(
            getFields()
                .stream()
                .map(Field::serialize)
                .collect(Collectors.toList())
        );
    }

    public void deserialize(String dump) {
        assert getFields() != null;

        List<Field<?>> fields = getFields();
        String[] data = CSV.split(dump);

        for(int i = 0; i < fields.size(); i++)
        {
            fields.get(i).deserialize(data[i]);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!super.equals(obj)) return false;
        Model model = (Model) obj;
        return getID() == model.getID();
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                super.hashCode(),
                getID()
        );
    }
}
