package Models;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import Fields.*;

import static Common.Utility.concatenate;

public class Meeting extends Task  {

    private DateField date = new DateField("date");
    private TimeField time = new TimeField("time");
    private StringField address = new StringField("address");
    private IntegerListField guests = new IntegerListField("guests");

    public Meeting() {
        super();
    }

    public Meeting(String name, String details, LocalDate date, LocalTime time, List<Integer> guests, String address) {
        super(name, details);

        this.date.setValue(date);
        this.time.setValue(time);
        this.address.setValue(address);
        this.guests.setValue(guests);
    }

    public LocalDate getDate() {
        return date.getValue();
    }

    public void setDate(LocalDate date) {
        this.date.setValue(date);
    }

    public LocalTime getTime() {
        return time.getValue();
    }

    public void setTime(LocalTime time) {
        this.time.setValue(time);
    }

    public String getAddress() {
        return address.getValue();
    }

    public void setAddress(String address) {
        this.address.setValue(address);
    }

    public List<Integer> getGuests() {
        return guests.getValue();
    }

    public void setGuests(List<Integer> guests) {
        this.guests.setValue(guests);
    }

    @Override
    public Meeting withName(String name) {
        return (Meeting) super.withName(name);
    }

    @Override
    public Meeting withDetails(String details) {
        return (Meeting) super.withDetails(details);
    }

    public Meeting withDate(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public Meeting withTime(LocalTime time) {
        this.setTime(time);
        return this;
    }

    public Meeting withAddress(String address) {
        this.setAddress(address);
        return this;
    }

    public Meeting withGuests(List<Integer> guests) {
        this.setGuests(guests);
        return this;
    }

    @Override
    public List<Field<?>> getFields() {
        return concatenate(
            super.getFields(),
            List.of(
                date, time, address, guests
            )
        );
    }
}
