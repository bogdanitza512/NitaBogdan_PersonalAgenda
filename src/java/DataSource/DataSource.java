package DataSource;

import io.github.cdimascio.dotenv.Dotenv;

import java.util.Properties;
import java.sql.*;


public class DataSource {

    private static DataSource instance = new DataSource();

    private String host;
    private String port;
    private String id;
    private String user;
    private String pass;

    public static DataSource getInstance() { return instance; }

    private DataSource() {
        Dotenv dotenv = Dotenv.configure()
                .ignoreIfMalformed()
                .ignoreIfMissing()
                .load();

        host = dotenv.get("DB_HOST");
        port = dotenv.get("DB_PORT");
        id = dotenv.get("DB_ID");
        user = dotenv.get("DB_USER");
        pass = dotenv.get("DB_PASS");
    }

    private Connection createConnection() throws SQLException {
        String template = "jdbc:postgresql://%s:%s/%s";
        String url = String.format(template, host, port, id);

        Properties props = new Properties();
        props.setProperty("user", user);
        props.setProperty("password", pass);

        return DriverManager.getConnection(url, props);
    }

    public Connection getConnection()  {
        Connection connection = null;
        try {
            connection = createConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public void closeConnection(Connection connection)  {
        try {
            if(connection != null) {
                connection.close();
            }
        }
        catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
