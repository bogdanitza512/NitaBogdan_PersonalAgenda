package GUI;

import javax.swing.*;
import java.sql.SQLException;

public class MeetingFrame extends JFrame {
    public MeetingFrame(String title) {
        super(title);
        JButton nav_btn = new JButton("Merg la ecran2");
        nav_btn.setBounds(5, 0, 150, 30);
        nav_btn.addActionListener(e -> {
            MeetingFrame frame = new MeetingFrame("Contacts");
            frame.setVisible(true);
            this.dispose();
        });
    }
}
