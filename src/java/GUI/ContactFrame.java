package GUI;

import Models.Contact;
import Services.ContactService;

import javax.swing.*;
import java.sql.*;

public class ContactFrame extends JFrame {
    public ContactFrame(String title) {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(720,480);
        this.setVisible(true);
        this.setResizable(false);
        this.setTitle("Test");

        JButton nav_btn = new JButton("Merg la ecran2");
        nav_btn.setBounds(5, 0, 150, 30);
        nav_btn.addActionListener(e -> {
            MeetingFrame frame = new MeetingFrame("Meetings");
            frame.setVisible(true);
            this.dispose();
        });
        JLabel firstNameLabel = new JLabel("First name: " );
        JTextField firstNameText = new JTextField();
        firstNameLabel.setBounds(5, 50, 200, 30);
        firstNameText.setBounds(150, 50, 200, 30);

        JLabel lastNameLabel = new JLabel("Last name: " );
        JTextField lastNameText = new JTextField();
        lastNameLabel.setBounds(5, 100, 200, 30);
        lastNameText.setBounds(150, 100, 200, 30);

        JLabel phoneNumberLabel = new JLabel("Phone number: " );
        JTextField phoneNumberText = new JTextField();
        phoneNumberLabel.setBounds(5, 150, 200, 30);
        phoneNumberText.setBounds(150, 150, 200, 30);

        JButton button = new JButton("Add Contact");
        button.setBounds(5, 450, 150, 30);
        button.addActionListener(event -> {
            ContactService.getInstance().create(
                    new Contact()
                            .withFirstName(firstNameText.getText())
                            .withLastName(firstNameText.getText())
                            .withPhoneNumber(phoneNumberText.getText())
            );
        });

        add(nav_btn);
        add(firstNameLabel);
        add(firstNameText);
        add(lastNameLabel);
        add(lastNameText);
        add(phoneNumberLabel);
        add(phoneNumberText);
    }
}
