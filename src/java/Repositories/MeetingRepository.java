package Repositories;

import Models.Meeting;

import java.sql.Connection;

public class MeetingRepository extends Repository<Meeting> {
    private final Connection connection;

    public MeetingRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public String getTableName() {
        return "meetings";
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public Meeting getDefaultModel() {
        return new Meeting();
    }
}
