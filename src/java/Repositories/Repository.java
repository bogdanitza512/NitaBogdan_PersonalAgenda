package Repositories;

import Fields.Field;
import Models.Model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Repository<T extends Model> {

    public abstract String getTableName();
    public abstract Connection getConnection();
    public abstract T getDefaultModel();

    public void create(T item) throws SQLException {
        assert getTableName() != null;
        assert getConnection() != null;

        String template = "INSERT INTO %s VALUES(%s)";
        String table = getTableName();
        String params = "?" + ", ?".repeat(item.getFields().size()-1);
        String sql = String.format(template, table, params);

        Connection connection = getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);

        int index = 1;
        for(Field<?> field : item.getFields()) {
            field.serialize(connection, statement, index);
            index++;
        }
        statement.executeUpdate();
    }

    public List<T> readAll() throws SQLException {
        assert getTableName() != null;
        assert getConnection() != null;
        assert getDefaultModel() != null;

        ArrayList<T> items = new ArrayList<>();

        String template = "SELECT * FROM %s";
        String table = getTableName();
        String sql = String.format(template, table);

        Connection connection = getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();

        ArrayList<T> result = parseResultSet(resultSet, connection);

        return result;
    }

    public List<T> read(Field<?> filter) throws SQLException {
        assert getTableName() != null;
        assert getConnection() != null;

        String template = "SELECT * FROM %s WHERE %s = ?";
        String table = getTableName();
        String key = filter.getKey();
        String sql = String.format(template, table, key);

        Connection connection = getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        filter.serialize(connection, statement, 1);
        ResultSet resultSet = statement.executeQuery();

        ArrayList<T> result = parseResultSet(resultSet, connection);

        return result;
    }

    public void update(Field<?> filter, Field<?> mutation) throws SQLException {
        assert getTableName() != null;
        assert getConnection() != null;

        String template = "UPDATE %s SET %s = ? WHERE %s = ?";
        String table = getTableName();
        String filterKey = filter.getKey();
        String mutationKey = mutation.getKey();
        String sql = String.format(template, table, filterKey, mutationKey);

        Connection connection = getConnection();
        PreparedStatement statement =  connection.prepareStatement(sql);
        filter.serialize(connection,statement, 1);
        mutation.serialize(connection,statement, 2);
        statement.executeUpdate();
    }

    public void delete(Field<?> filter) throws SQLException {
        assert getTableName() != null;
        assert getConnection() != null;

        String template = "DELETE FROM %s WHERE %s = ?";
        String table = getTableName();
        String key = filter.getKey();
        String sql = String.format(template, table, key);

        Connection connection = getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        filter.serialize(connection, statement, 1);
        statement.executeUpdate();
    }

    public void deleteAll() throws SQLException {
        assert getTableName() != null;
        assert getConnection() != null;

        String template = "DELETE FROM %s WHERE 1 = 1";
        String table = getTableName();
        String sql = String.format(template, table);

        Connection connection = getConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.executeUpdate();
    }

    private ArrayList<T> parseResultSet(ResultSet resultSet, Connection connection) throws SQLException {
        ArrayList<T> result = new ArrayList<>();

        while(resultSet.next()) {
            T item = getDefaultModel();

            int index = 1;
            for (Field<?> field  : item.getFields()) {
                field.deserialize(connection, resultSet, index);
                index++;
            }
            result.add(item);
        }

        return result;
    }
}
