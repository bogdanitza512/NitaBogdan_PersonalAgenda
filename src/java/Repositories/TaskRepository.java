package Repositories;

import Models.Task;

import java.sql.Connection;

public class TaskRepository extends Repository<Task> {
    private final Connection connection;

    public TaskRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public String getTableName() {
        return "tasks";
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public Task getDefaultModel() {
        return new Task();
    }
}