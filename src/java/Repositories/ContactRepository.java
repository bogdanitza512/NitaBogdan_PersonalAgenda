package Repositories;

import Models.Contact;

import java.sql.*;

public class ContactRepository extends Repository<Contact> {
    private final Connection connection;

    public ContactRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public String getTableName() {
        return "contacts";
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public Contact getDefaultModel() {
        return new Contact();
    }

}
