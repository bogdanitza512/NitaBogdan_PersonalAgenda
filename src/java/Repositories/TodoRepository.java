package Repositories;

import Models.Todo;

import java.sql.Connection;

public class TodoRepository extends Repository<Todo> {
    private final Connection connection;

    public TodoRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public String getTableName() {
        return "todos";
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public Todo getDefaultModel() {
        return new Todo();
    }
}
