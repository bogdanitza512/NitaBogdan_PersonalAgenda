package Repositories;

import Models.Reminder;

import java.sql.Connection;

public class ReminderRepository extends Repository<Reminder> {
    private final Connection connection;

    public ReminderRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public String getTableName() {
        return "reminders";
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public Reminder getDefaultModel() {
        return new Reminder();
    }
}
