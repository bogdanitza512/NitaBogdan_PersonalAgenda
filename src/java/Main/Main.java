package Main;
import GUI.ContactFrame;
import Models.Contact;
import Models.Meeting;
import Models.Reminder;
import Models.Todo;
import Services.ContactService;
import Services.MeetingService;
import Services.ReminderService;
import Services.TodoService;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        int count = 10;
        int origin = 0;
        int bound = 100;
        testContactService(count, origin, bound);
//        testMeetingService(count, origin, bound);
//        testReminderService(count, origin, bound);
//        testTodoService(count, origin, bound);

        ContactService contactService = ContactService.getInstance();
        contactService.create(new Contact().withFirstName("dasda"));
        System.out.println(contactService.read(10));
        contactService.delete(17);

//        ContactFrame contactsFrame = new ContactFrame("Contacts");
    }

    public static void testContactService(int count, int origin, int bound)
    {
        Random random = new Random();

        System.out.println("TEST-CONTACT-SERVICE");

        ContactService contactService = ContactService.getInstance();

        contactService.load();

        for(int i = 1; i < count - 1; i++)
        {
            contactService.create(
                new Contact()
                    .withFirstName(String.format("FirstName_%02d", random.nextInt(bound)))
                    .withLastName(String.format("LastName_%02d", random.nextInt(bound)))
                    .withPhoneNumber(
                        String.format(
                            "07%s",
                            random
                                .ints(8, 0,9)
                                .boxed()
                                .map(Objects::toString)
                                .collect(Collectors.joining())
                        ))
            );
        }

        System.out.println(contactService.readAll());

        contactService.save();
    }

    public static void testMeetingService(int count, int origin, int bound)
    {
        Random random = new Random();

        System.out.println("TEST-MEETING-SERVICE");

        MeetingService meetingService = MeetingService.getInstance();

        meetingService.load();

        for(int i = 1; i < count - 1; i++)
        {
            meetingService.create(
                new Meeting()
                    .withName(String.format("Meeting_%02d", random.nextInt(bound)))
                    .withDetails(String.format("Details_%02d", random.nextInt(bound)))
                    .withAddress(String.format("Address_%02d", random.nextInt(bound)))
                    .withDate(LocalDate.now().plusDays(random.nextInt(bound)))
                    .withTime(LocalTime.now().plusHours(random.nextInt(bound)))
                    .withGuests(
                        random
                            .ints(origin,bound)
                            .limit(random.nextInt(count))
                            .boxed()
                            .collect(Collectors.toList())
                    )
            );
        }

        System.out.println(meetingService.readAll());

        meetingService.save();
    }

    public static void testReminderService(int count, int origin, int bound)
    {
        Random random = new Random();

        System.out.println("TEST-MEETING-SERVICE");

        ReminderService reminderService = ReminderService.getInstance();

        reminderService.load();

        for(int i = 1; i < count - 1; i++)
        {
            reminderService.create(
                new Reminder()
                    .withName(String.format("Reminder_%02d", random.nextInt(bound)))
                    .withDetails(String.format("Details_%02d", random.nextInt(bound)))
                    .withStartDate(LocalDate.now().minusDays(random.nextInt(bound)))
                    .withInterval(Duration.ofHours(12 * random.nextInt(bound)))
                    .withEndDate(LocalDate.now().plusDays(random.nextInt(bound)))
            );
        }

        System.out.println(reminderService.readAll());

        reminderService.save();
    }

    public static void testTodoService(int count, int origin, int bound)
    {
        Random random = new Random();

        System.out.println("TEST-MEETING-SERVICE");

        TodoService todoService = TodoService.getInstance();

        todoService.load();

        for(int i = 1; i < count - 1; i++)
        {
            todoService.create(
                new Todo()
                    .withName(String.format("Reminder_%02d", random.nextInt(bound)))
                    .withDetails(String.format("Details_%02d", random.nextInt(bound)))
                    .withDeadline(LocalDate.now().minusDays(1))
                    .withSubTasks(
                        random
                            .ints(origin,bound)
                            .limit(random.nextInt(count))
                            .boxed()
                            .collect(Collectors.toList())
                    )
            );
        }

        System.out.println(todoService.readAll());

        todoService.save();
    }
}
