package Fields;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;


public class DateField extends Field<LocalDate> {

    private DateTimeFormatter format = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);

    public DateField(String key, LocalDate value) {
        super(key, value);
    }

    public DateField(String key) {
        super(key);
    }

    public DateTimeFormatter getFormat() {
        return format;
    }

    public void setFormat(DateTimeFormatter format) {
        this.format = format;
    }

    public DateField withFormat(DateTimeFormatter format) {
        this.setFormat(format);
        return this;
    }

    @Override
    public DateField withValue(LocalDate value) {
        setValue(value);
        return this;
    }

    @Override
    protected LocalDate getDefaultValue() {
        return LocalDate.now();
    }

    @Override
    public String serialize() {
        return getFormat().format(getValue());
    }

    @Override
    public void serialize(Connection connection, PreparedStatement statement, int index) throws SQLException {
        LocalDate value = getValue();
        statement.setDate(index, Date.valueOf(value));
    }

    @Override
    public void deserialize(String dump) {
        setValue(LocalDate.parse(dump, getFormat()));
    }

    @Override
    public void deserialize(Connection connection, ResultSet resultSet, int index) throws SQLException {
        Date value = resultSet.getDate(index);
        setValue(value.toLocalDate());
    }

}
