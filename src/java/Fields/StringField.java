package Fields;

import Common.Utility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StringField extends Field<String> {

    public StringField(String key, String value) { super(key, value); }

    public StringField(String key) { super(key); }

    @Override
    public StringField withValue(String value) {
        setValue(value);
        return this;
    }

    @Override
    protected String getDefaultValue() {
        return "";
    }

    @Override
    public String serialize() {
        return getValue();
    }

    @Override
    public void serialize(Connection connection, PreparedStatement statement, int index) throws SQLException {
        String value = getValue();
        statement.setString(index, value);
    }

    @Override
    public void deserialize(String dump) {
        this.setValue(dump);
    }

    @Override
    public void deserialize(Connection connection, ResultSet resultSet, int index) throws SQLException {
        String value = resultSet.getString(index);
        setValue(value);
    }
}