package Fields;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class IntegerField extends Field<Integer> {
    public IntegerField(String key, Integer value) { super(key, value); }

    public IntegerField(String key) { super(key); }

    @Override
    public IntegerField withValue(Integer value) {
        setValue(value);
        return this;
    }

    @Override
    protected Integer getDefaultValue() {
        return 0;
    }

    @Override
    public String serialize() {
        return Integer.toString(getValue());
    }

    @Override
    public void serialize(Connection connection, PreparedStatement statement, int index) throws SQLException {
        statement.setInt(index, getValue());
    }

    @Override
    public void deserialize(String dump) {
        this.setValue(Integer.parseInt(dump));
    }

    @Override
    public void deserialize(Connection connection, ResultSet resultSet, int index) throws SQLException {
        setValue(resultSet.getInt(index));
    }
}
