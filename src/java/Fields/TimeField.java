package Fields;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class TimeField extends Field<LocalTime> {

    private DateTimeFormatter format = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);

    public TimeField(String key, LocalTime value) {
        super(key, value);
    }

    public TimeField(String key) {
        super(key);
    }

    public DateTimeFormatter getFormat() {
        return format;
    }

    public void setFormat(DateTimeFormatter format) {
        this.format = format;
    }

    public TimeField withFormat(DateTimeFormatter format) {
        this.setFormat(format);
        return this;
    }

    @Override
    public TimeField withValue(LocalTime value) {
        setValue(value);
        return this;
    }

    @Override
    protected LocalTime getDefaultValue() {
        return LocalTime.now();
    }

    @Override
    public String serialize() {
        return getValue().format(getFormat());
        //return getFormat().format(getValue());
    }

    @Override
    public void serialize(Connection connection, PreparedStatement statement, int index) throws SQLException {
        LocalTime value = getValue();
        statement.setTime(index, Time.valueOf(value));
    }

    @Override
    public void deserialize(String dump) {
        setValue(LocalTime.parse(dump, getFormat()));
    }

    @Override
    public void deserialize(Connection connection, ResultSet resultSet, int index) throws SQLException {
        Time value = resultSet.getTime(index);
        setValue(value.toLocalTime());
    }
}
