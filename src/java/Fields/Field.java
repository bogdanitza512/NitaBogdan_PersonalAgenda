package Fields;

import java.sql.*;

public abstract class Field<T> implements Cloneable {
    private String key;
    private T value;

    public abstract Field<T> withValue(T value);
    public abstract String serialize();
    public abstract void serialize(Connection connection, PreparedStatement statement, int index) throws SQLException;

    public abstract void deserialize(String dump);
    public abstract void deserialize(Connection connection, ResultSet resultSet, int index) throws SQLException;


    protected abstract T getDefaultValue();

    public Field(String key) {
        this.key = key;
        this.value = getDefaultValue();
    }

    public Field(String key, T value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return this.key;
    };

    public T getValue() {
        return this.value;
    };

    public void setValue(T value) {
        this.value = value;
    };

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
