package Fields;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;

public class DurationField extends Field<Duration> {
    public DurationField(String key, Duration value) {
        super(key, value);
    }

    public DurationField(String key) {
        super(key);
    }

    @Override
    public DurationField withValue(Duration value) {
        setValue(value);
        return this;
    }

    @Override
    protected Duration getDefaultValue() {
        return Duration.ZERO;
    }

    @Override
    public String serialize() {
        return getValue().toString();
    }

    @Override
    public void serialize(Connection connection, PreparedStatement statement, int index) throws SQLException {
        Duration value = getValue();
        statement.setLong(index, value.toSeconds());
    }

    @Override
    public void deserialize(String dump) {
        setValue(Duration.parse(dump));
    }

    @Override
    public void deserialize(Connection connection, ResultSet resultSet, int index) throws SQLException {
        Long durationInSeconds = resultSet.getLong(index);
        setValue(Duration.ofSeconds(durationInSeconds));

    }
}
