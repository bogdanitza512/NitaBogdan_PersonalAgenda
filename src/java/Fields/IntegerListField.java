package Fields;

import java.sql.*;
import java.util.List;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Vector;
import java.util.stream.Collector;
import java.util.stream.Collectors;


public class IntegerListField extends Field<List<Integer>> {

    public IntegerListField(String key, List<Integer> value) {
        super(key, value);
    }

    public IntegerListField(String key) {
        super(key);
    }

    @Override
    public IntegerListField withValue(List<Integer> value) {
        setValue(value);
        return this;
    }

    @Override
    protected List<Integer> getDefaultValue() {
        return new Vector<>();
    }

    @Override
    public String serialize() {
        return getValue().toString().replace(", ", " ");
    }

    @Override
    public void serialize(Connection connection, PreparedStatement statement, int index) throws SQLException {
        Object[] values = getValue().toArray();
        Array array = connection.createArrayOf("INTEGER", values);
        statement.setArray(index, array);
    }

    @Override
    public void deserialize(String dump) {
        String input = dump.substring(1, dump.length()-1);
        Scanner scanner = new Scanner(input);
        while (scanner.hasNextInt()) {
            int val = scanner.nextInt();
            getValue().add(val);
        }
    }

    @Override
    public void deserialize(Connection connection, ResultSet resultSet, int index) throws SQLException {
        Array sqlArray = resultSet.getArray(index);
        Object[] objectArray = (Object[]) sqlArray.getArray();
        List<Integer> integerList = Arrays.stream(objectArray)
                .map(Object::toString)
                .map(Integer::valueOf)
                .collect(Collectors.toList());
        setValue(integerList);
    }


}
