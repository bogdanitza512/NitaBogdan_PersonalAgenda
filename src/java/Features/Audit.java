package Features;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import Fields.Field;
import Fields.StringField;
import Fields.TimeField;
import Models.Model;

public class Audit {
    private static Set<Audit> instances = new HashSet<>();

    public static Audit getInstance(String resource) {
        assert resource != null;

        Audit instance = null;

        for(Audit item : instances)
        {
            if(item.resource.equals(resource))
            {
                instance = item;
            }
        }

        if(instance == null)
        {
            instance = new Audit(resource);
            instances.add(instance);
        }

        return  instance;
    }

    private static final String postfix = "audit";
    private static final String extension = "csv";

    private static final String WARNING = "warning";
    private static final String ERROR = "error";
    private static final String INFO = "info";

    private String resource;

    private Audit(String resource) {
        this.resource = resource;
        this.write(new Entry().getHeader(), false);
    }

    public String getPath() {
        return resource + "_" + postfix + "." + extension;
    }

    public void warning (String message) {
        log(WARNING, message);
    }

    public void error (String message) {
        log(ERROR, message);
    }

    public void info (String message) {
        log(INFO, message);
    }

    private void log(String type, String message){
        Entry entry = new Entry(LocalTime.now(), type, message);
        write(entry.serialize(), true);
    }

    private void write(String dump, boolean append){
        FileWriter fw = null;
        try {
            fw = new FileWriter(getPath(), append);
            fw.append(dump);
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                assert fw != null;
                fw.flush();
                fw.close();
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class Entry extends Model {
        private TimeField timestamp = new TimeField("timestamp")
            .withFormat(DateTimeFormatter.ofPattern("HH:mm:ss.SSS"));
        private StringField type = new StringField("type").withValue("N/A");
        private StringField message = new StringField("message").withValue("N/A");

        public Entry() { super(); }

        public Entry(LocalTime timestamp, String type, String message) {
            this.timestamp.setValue(timestamp);
            this.type.setValue(type);
            this.message.setValue(message);
        }

        @Override
        public int getID() { return 0; }
        @Override
        public List<Field<?>> getFields() {
            return List.of(
                timestamp, type, message
            );
        }
    }

}
