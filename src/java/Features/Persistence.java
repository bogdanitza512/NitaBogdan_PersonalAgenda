package Features;

import Models.Model;
import Services.Service;

import java.io.*;
import java.util.List;

public interface Persistence<T extends Model> {

    final String postfix = "persistence";
    final String extension = "csv";

    public Service<T> getModelService();

    default public String getFileName() {
        return this.getClass().getName().toLowerCase();
    }

    private String getFilePath() {
        assert getFileName() != null;
        return getFileName() + "_" + postfix + "." + extension;
    }

    default String getModelHeader() {
        assert getModelService() != null;
        return getModelService().getModelInstance().getHeader();
    }

    public default void load() {
        File input = new File(getFilePath());
        if(input.length() == 0) return;

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(input));
            if(getModelHeader() != null) br.skip(getModelHeader().length());

            String line;
            while ((line = br.readLine()) != null) {
                T item = getModelService().getModelInstance();
                item.deserialize(line);
                getModelService().create(item);
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        } finally {
            try {
                assert br != null;
                br.close();
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    public default void save() {
        FileWriter fw = null;
        List<T> content = getModelService().readAll();

        try {
            fw = new FileWriter(getFilePath());
            fw.append(getModelHeader());

            for (T item : content) {
                fw.append(item.serialize());
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                assert fw != null;

                fw.flush();
                fw.close();
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        }
    }


}


