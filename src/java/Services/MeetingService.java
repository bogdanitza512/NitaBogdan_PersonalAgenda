package Services;

import DataSource.DataSource;
import Features.Persistence;
import Models.Meeting;
import Repositories.MeetingRepository;
import Repositories.Repository;

import java.sql.Connection;

public class MeetingService extends Service<Meeting> implements Persistence<Meeting> {
    private static MeetingService instance = new MeetingService();
    public static MeetingService getInstance() { return instance; }

    private final Connection connection = DataSource.getInstance().getConnection();
    private MeetingRepository repository = new MeetingRepository(connection);

    private MeetingService() { }

    @Override
    public Service<Meeting> getModelService() {
        return MeetingService.getInstance();
    }

    @Override
    public Repository<Meeting> getRepository() {
        return repository;
    }
}
