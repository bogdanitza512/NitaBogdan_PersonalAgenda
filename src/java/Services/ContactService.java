package Services;

import DataSource.DataSource;
import Features.Persistence;
import Models.Contact;
import Repositories.ContactRepository;
import Repositories.Repository;

import java.sql.Connection;

public class ContactService extends Service<Contact> implements Persistence<Contact> {
    private static ContactService instance = new ContactService();
    public static ContactService getInstance() { return instance; }

    private final Connection connection = DataSource.getInstance().getConnection();
    private ContactRepository repository = new ContactRepository(connection);

    private ContactService() { }

    @Override
    public Repository<Contact> getRepository() {
        return repository;
    }

    @Override
    public Service<Contact> getModelService() {
        return ContactService.getInstance();
    }
}
