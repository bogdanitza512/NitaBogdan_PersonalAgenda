package Services;

import Features.Audit;
import Fields.IntegerField;
import Models.Model;
import Repositories.Repository;

import java.sql.SQLException;
import java.util.List;

public abstract class Service<T extends Model > {

    abstract protected Repository<T> getRepository();
    protected final Audit log = Audit.getInstance(
            this.getClass().getName().toLowerCase()
    );

    public T getModelInstance() {
        assert getRepository() != null;
        return getRepository().getDefaultModel();
    }

    public void create(T item) {
        assert getRepository() != null;
        log.info("create called");
        try {
            getRepository().create(item);
        }
        catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public List<T> readAll() {
        assert getRepository() != null;
        log.info("read all called");
        try {
            return getRepository().readAll();
        }
        catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public List<T> read(int id) {
        assert getRepository() != null;
        log.info("read called");
        try {
            return getRepository().read(
                    new IntegerField("id").withValue(id)
            );
        }
        catch (SQLException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public void deleteAll() {
        assert getRepository() != null;
        log.info("read all called");
        try {
            getRepository().deleteAll();
        }
        catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public void delete(int id) {
        assert getRepository() != null;
        log.info("read called");
        try {
            getRepository().delete(
                    new IntegerField("id").withValue(id)
            );
        }
        catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

}
