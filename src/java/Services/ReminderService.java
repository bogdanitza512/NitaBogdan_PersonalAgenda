package Services;

import DataSource.DataSource;
import Features.Persistence;
import Models.Reminder;
import Repositories.ReminderRepository;
import Repositories.Repository;

import java.sql.Connection;

public class ReminderService extends Service<Reminder> implements Persistence<Reminder> {
    private static ReminderService instance = new ReminderService();
    public static ReminderService getInstance() { return instance; }

    private final Connection connection = DataSource.getInstance().getConnection();
    private ReminderRepository repository = new ReminderRepository(connection);

    private ReminderService() { }

    @Override
    public Service<Reminder> getModelService() {
        return ReminderService.getInstance();
    }

    @Override
    public Repository<Reminder> getRepository() {
        return repository;
    }
}
