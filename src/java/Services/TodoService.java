package Services;

import DataSource.DataSource;
import Features.Persistence;
import Models.Model;
import Models.Todo;
import Repositories.Repository;
import Repositories.TodoRepository;

import java.sql.Connection;

public class TodoService extends Service<Todo> implements Persistence<Todo> {
    private static TodoService instance = new TodoService();
    public static TodoService getInstance() { return instance; }

    private final Connection connection = DataSource.getInstance().getConnection();
    private TodoRepository repository = new TodoRepository(connection);

    private TodoService() { }

    @Override
    public Service<Todo> getModelService() {
        return TodoService.getInstance();
    }

    @Override
    protected Repository<Todo> getRepository() {
        return repository;
    }
}
