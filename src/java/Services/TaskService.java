package Services;

import DataSource.DataSource;
import Features.Persistence;
import Models.Task;
import Repositories.Repository;
import Repositories.TaskRepository;

import java.sql.Connection;

public class TaskService extends Service<Task> implements Persistence<Task> {

    private static TaskService instance = new TaskService();
    public static TaskService getInstance() { return instance; }

    private final Connection connection = DataSource.getInstance().getConnection();
    private TaskRepository repository = new TaskRepository(connection);

    private TaskService() { }

    @Override
    public Repository<Task> getRepository() {
        return repository;
    }

    @Override
    public Service<Task> getModelService() {
        return TaskService.getInstance();
    }
}
